1. Membuat Database

CREATE DATABASE myshop; 

2. Membuat Table di Dalam Database

CREATE TABLE users(id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), email VARCHAR(255), password VARCHAR(255));
CREATE TABLE items(id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), description VARCHAR(255), price INT, stok INT, category_id INT);
CREATE TABLE categories(id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255));

3. Memasukkan Data pada Table

INSERT INTO `myshop`.`users` (`id`, `name`, `email`, `password`) VALUES (NULL, 'John Doe', 'john@doe.com', 'john123');
INSERT INTO `myshop`.`users` (`id`, `name`, `email`, `password`) VALUES (NULL, 'Jane Doe', 'jane@doe.com', 'jenita123');

INSERT INTO `myshop`.`categories` (`id`, `name`) VALUES (NULL, 'gadget');
INSERT INTO `myshop`.`categories` (`id`, `name`) VALUES (NULL, 'cloth');
INSERT INTO `myshop`.`categories` (`id`, `name`) VALUES (NULL, 'men');
INSERT INTO `myshop`.`categories` (`id`, `name`) VALUES (NULL, 'women');
INSERT INTO `myshop`.`categories` (`id`, `name`) VALUES (NULL, 'branded');

INSERT INTO `myshop`.`items` (`id`, `name`, `description`, `price`, `stok`, `category_id`) VALUES (NULL, 'Sumsang b50', 'hape keren dari merek sumsang', '4000000', '100', '1');
INSERT INTO `myshop`.`items` (`id`, `name`, `description`, `price`, `stok`, `category_id`) VALUES (NULL, 'Uniklooh', 'baju keren dari brand ternama', '500000', '50', '2');
INSERT INTO `myshop`.`items` (`id`, `name`, `description`, `price`, `stok`, `category_id`) VALUES (NULL, 'IMHO Watch', 'jam tangan anak yang jujur banget', '2000000', '10', '1');

4. Mengambil Data dari Database

   a. Mengambil data users
      SELECT id, name, email FROM `users`

   b. Mengambil data items
      SELECT * FROM `items` WHERE price>1000000
      SELECT * FROM `items` WHERE name LIKE '%sang%'

   c. Menampilkan data items join dengan kategori
      SELECT items.name, items.description, items.stok, items.category_id, categories.name FROM items JOIN categories ON items.id = categories.id

5. Mengubah Data dari Database
UPDATE items SET price=2500000 WHERE id=1